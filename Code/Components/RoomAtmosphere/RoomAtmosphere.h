#pragma once 

#include "StdAfx.h"

#include <CrySchematyc/CoreAPI.h>

class CAtmosphereData
{
public:
	struct SGas
	{
		inline bool operator==(const SGas& r) const
		{
			return 0 == memcmp(this, &r, sizeof(r));
		}

		inline bool operator!=(const SGas& r) const
		{
			return 0 != memcmp(this, &r, sizeof(r));
		}

		static void ReflectType(Schematyc::CTypeDesc<SGas>& d)
		{
			d.SetGUID("{8C25621F-7E28-4655-AB2A-F4F0691E4322}"_cry_guid);
			//d.AddMember(&SGas::m_name, 'name', "GasName", "Gas Name", "The name of the Gas", "");
			//d.AddMember(&SGas::m_gasSymbol, 'symb', "GasSymbol", "Gas Symbol", "The molecular symbol of the Gas", "");
			//d.AddMember(&SGas::m_atmoComp, 'comp', "AtmoComp", "Atmo Comp", "What percent of the atmosphere the gas occupies", 0.f);
		}

		void Serialize(Serialization::IArchive& a)
		{
			a(m_name, "name", "Name");
			a.doc("Name of Gas");
			a(m_gasSymbol, "gassymbol", "Molecular symbol of Gas");
			a.doc("Molecular symbol");
			a(m_atmoComp, "atmocomp", "The percentage of the Atmosphere the Gas makes up");
			a.doc("Relative comp in atmo");
		}

		Schematyc::CSharedString m_name;
		Schematyc::CSharedString m_gasSymbol;
		float m_atmoComp;
	};

	struct SClimate
	{
		inline bool operator==(const SClimate& r) const
		{
			return 0 == memcmp(this, &r, sizeof(r));
		}

		inline bool operator!=(const SClimate& r) const
		{
			return 0 != memcmp(this, &r, sizeof(r));
		}

		static void ReflectType(Schematyc::CTypeDesc<SClimate>& d)
		{
			d.SetGUID("{43D0E795-5A36-40EF-9246-8AC7538C37BF}"_cry_guid);
			d.AddMember(&SClimate::m_temp, 'temp', "Temp", "Temperature", "Temperature in the Room in Celsius", .0f);
			d.AddMember(&SClimate::m_humidity, 'humi', "Humidity", "Humidity", "Relative humidity in the Room", .0f);
		}

		float m_temp;
		float m_humidity;
	};

	struct SAtmosphere
	{
		typedef Schematyc::CArray<SGas> GasArray;
		inline bool operator==(const SAtmosphere& r) const
		{
			return 0 == memcmp(this, &r, sizeof(r));
		}

		inline bool operator!=(const SAtmosphere& r) const
		{
			return 0 != memcmp(this, &r, sizeof(r));
		}

		static void ReflectType(Schematyc::CTypeDesc<SAtmosphere>& r)
		{
			r.SetGUID("{C6FD3F20-6CE2-46C2-9D94-E0D80B2FEFAE}"_cry_guid);
			r.SetLabel("Gasses");
			r.SetDescription("The Gasses in the Room");
			r.AddMember(&SAtmosphere::m_gasses, 'gass', "Gasses", "Gasses", "The gasses in the Room", GasArray());
			r.AddMember(&SAtmosphere::m_climate, 'clim', "Climate", "Climate", "The Climate of the Room", SClimate());
		}

		GasArray m_gasses;
		SClimate m_climate;
	};
};